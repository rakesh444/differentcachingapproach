# In this article we will discuss how caching can help to improve performance of an application :
In an application there is lot of request is made from client side to server side, and in some cases performance of our application may go down due to that much client request. Then we can increase performance of our application by the help of caching. Caching is basically a memory in which most frequent used data is stored , so that in near future if we need previously used data again then we can directly fetch it from cach memory instead of searching it again in main memory or main database of our application.
>There are many approach by which we can implement caching in our application , but we need to use that approach which is most suited on our application otherwise things may go wrong.


Caching approach should depend on how data are read/written in our application. For example we need to see

- is application  write heavy and reads less frequently? 
- is data written once and read multiple times? 
- is data returned always unique?

# Different caching approaches :

## 1. Cache-Aside -
This is most commonly used caching approach. In this approach cache memory is not directly connected with main database, cache memory is placed in seperate line. Whenever application needs any data it first search in cache memory if data is found then it is called cache-hit and directly fetched from cache memory and if data is not found in cache memory then it is called cache-miss and in this case data is first fetched from main database and after that data will stored in cache memory such that if in near future that data is needed again then without taking much time directly fetch data from cache memory.
##### Pros and Cons
Cache-Aside approach is work best for application in which reading of data is happen many times. Another advantage of this approach is that data model of cache memory can be different than database model. In this approach if cache memory go down then also we can work directly with main database because in this method cache memory is stored in seperate line. 
Disadvantage of this approach is that using this approach database can be inconsistent, because our most commonly  write strategy is to write data to the database directly. When this happens, cache may become inconsistent with the database.    

## 2. Read-Through Cache -
Read-through cache sits in-line with the database. When there is a cache miss, it loads missing data from database, populates the cache and returns it to the application.
Both cache-aside and read-through strategies load data lazily, only when it is first read.
##### Pros and Cons 
Read-through caches also work best for read-heavy workloads when the same data is requested many times. In this approach both cache and main database is placed in-line, so we can not create different data for cache memory and different data model for main database. When the data will read it always results in cache-miss. Just like cache-aside, here also it is also possible for data to become inconsistent between cache and the database.

## 3. Write-Through Cache -
In the approach also cache memory is located in-line with database. In this write strategy, data is first written to the cache and then to the database. 
##### Pros and Cons
 In this approach cache sits in-line with the database and writes always go through the cache to the main database. So in this method we get database consistency guarantee along with this we can also get all the feature of read-through approach.
 
## 4. Write-Around -
In this approach data will directly stored in main database and when data will read then that data will store in cache memory. This approach will provides good performance in situations where data is written once and read less frequently or never. 

## 5. Write-Back -
In this approach the application writes data to the cache which acknowledges immediately and after some delay, it writes the data back to the database.

##### Pros and Cons
Write back caches improve the write performance and are good for write-heavy workloads. When combined with read-through, it works good for mixed workloads, where the most recently updated and accessed data is always available in cache.

# Summary -
Here, we explored different caching approach and their pros and cons. So if we want to implement caching in our application then first we need to understand data access pattern of our application then implement best one.
 





   
   
   
   
   
   
   
   
   
   
